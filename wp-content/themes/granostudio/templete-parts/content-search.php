<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */
?>

<div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all" data-0="transform:scale(0.4);opacity:.6;" data-end="transform:scale(1);opacity:1;">
  <a href="" class="image">
    <img src="http://via.placeholder.com/350x350?text=pagina1" class="rz">
    <div class="caption">
      <span class="anima">
        <span class="icon grano-branco logo-grano">Grano</span>
        <span class="title"><?php echo get_the_title(); ?></span>
        <span class="hashtags"><?php echo the_category(); ?></span>
      </span>
    </div>
  </a>
</div>
