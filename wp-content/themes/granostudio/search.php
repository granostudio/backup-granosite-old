<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header(); ?>


<section class="section section-1" id="section-1" style="margin-top: 100px;">
    <div class="container">
      <div class="row content-search">
        <div class="col-xs-12">
            <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="form-group">
              <div class="input-group input-group-unstyled">
                <input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
                <span class="input-group-addon">
                  <i class="fa fa-search"></i>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

<section class="section section-portfolio" id="portfolio-items" style="background-color: #FAFAFA;">
  <div class="container">
    <div class="row content-center mgt50">
    <div class="portfolio-grid blog-02" id="portfolio" data-anchor-target="#portfolio-items" data-top-bottom="top:0;" data-bottom-top="top:-50px;">

    <header class="page-header">
      <h1 class="page-title"><?php printf( __( 'Resultado de: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
      </header><!-- .page-header -->

      <div class="row portfolio-grid section-blog" data-anchor-target="#blog" data-bottom-top="top:50px;margin-top:0px;" data-top-bottom="top:-120px;margin-top:0;">

      <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
          $args = array( 'post_type' => 'post', 'posts_per_page' => 12, 'paged' => $paged, 'page' => $paged);
      ?>
      <?php
      if( have_posts() ) {
        while ( have_posts() ) {
          the_post();?>

          <div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all">
  					<a href="<?php echo get_the_permalink(); ?>" class="image">
  						<?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail( ); ?>
  							<div class="caption">
  								<span class="anima">
  									<span class="icon grano-branco logo-grano">Grano</span>
  									<span class="title"><?php echo get_the_title(); ?></span>
  									<ul class="lista-categoria">
  										<?php
  										foreach((get_the_category()) as $category) {
  											echo '<li class="hashtags">' . $category->cat_name . '</li>';
  										}
  										?>
  									</ul>
  								</span>
  							</div>

              <?php else: ?>
                <div class="foto"></div>
  							<div class="caption no-mask">
  								<span class="anima">
  									<span class="icon grano-branco logo-grano">Grano</span>
  									<span class="title"><?php echo get_the_title(); ?></span>
  									<ul class="lista-categoria">
  										<?php
  										foreach((get_the_category()) as $category) {
  											echo '<li class="hashtags">' . $category->cat_name . '</li>';
  										}
  										?>
  									</ul>
  								</span>
  							</div>
              <?php endif; ?>
  					</a>
  				</div>


      <?php
        }
      }
       else {
        echo "nada encontrado";
      }
       ?>

     </div>

   </div>
 </div>
</div>
</section>

<?php get_footer(); ?>
