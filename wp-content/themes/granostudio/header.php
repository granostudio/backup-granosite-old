<?php

/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

header("Content-Type: text/html; charset=utf-8",true);


if ($_SERVER['HTTP_HOST'] == '127.0.0.1'){
	define("ROOT_FOLDER", "/");
	// define("ROOT_PATH", "http://frontfe.com/client/seuemporio/public");
	define("ROOT_PATH", "");
	define("MEDIA_PATH", ROOT_PATH."grano-site/wp-content/themes/granostudio/media");

}elseif($_SERVER['HTTP_HOST'] == 'granostudio.com.br'){
	define("ROOT_FOLDER", "/");
	// define("ROOT_PATH", "http://apps.frontfe.com/grano-studio");
	define("ROOT_PATH", "");
	define("MEDIA_PATH", ROOT_PATH."wp-content/themes/granostudio/media");
}


if(isset($_REQUEST['language'])){
	$lang		= $_REQUEST['language'];
}else{
	$lang		= substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	if($lang == "pt-br" || $lang == "pt-pt" || $lang == "pt"){
		$lang	= "br";
	}
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
	<title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name'); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<meta name="robots" content="index, follow" />

	<base href="<?php echo ROOT_PATH ?>/"  lang="<?php echo $lang ?>" />

	<link rel="shortcut icon" href="<?php echo MEDIA_PATH; ?>/imgs/icons/grano.png" />


	<!-- SEO -->

	  <?php
	  // site title
	  $blog_title = get_bloginfo( 'name' );
	  if(is_home( )){
	  ?>
	  <meta property="og:title" content="<?php echo $blog_title; ?>" />
	  <?php
	    } else{
	  ?>
	  <meta property="og:title" content="<?php echo get_the_title(); ?>" />
	  <?php } ?>

	  <meta property="og:type" content="website" />

	  <?php if(is_home(	)){
		// site description
		$blog_desc = get_bloginfo( 'description' );
		?>
		<meta property="og:description" content="<?php echo $blog_desc; ?>" />
		<?php
		} else{
			?>
		<meta property="og:description" content="<?php echo get_the_content(); ?>" />
			<?php
		}
		 ?>

	  <meta property="og:site_name" content="Grano Studio" />	



	  	<?php
		//imagem default ou thumbnail post
		$urlimg = '';
		$id = get_the_ID();
		if ( has_post_thumbnail($id) ) {
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
			 	$urlimg = $large_image_url[0];
		 } else {
			 $urlimg = "http://granostudio.com.br/wp-content/themes/granostudio/media/imgs/logo.png";
		}
		 ?>
        <meta property="og:image" content="<?php echo $urlimg; ?>" />
        <meta property="og:image:type" content="image/jpeg" />
        <meta property="og:image:width" content="200" />
        <meta property="og:image:height" content="200" />




	<!-- METATAGS -->
	<meta property="fb:app_id" content="1932839393611611" />

	<link rel="stylesheet" href="<?php echo MEDIA_PATH; ?>/css/style.css" />

	<script src="https://use.fontawesome.com/cc34e9ff4f.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-70037167-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-70037167-1');
	</script>


  <?php wp_head(); ?>
</head>

<!-- <body class="page-home" style="position: absolute; top:0;"> -->
  <header class="navbar-fixed-top" id="nav-main" style="position: fixed;">
    <div class="container-fluid">
      <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://granostudio.com.br/">
            <img src="<?php echo MEDIA_PATH; ?>/imgs/logo.png" class="">
          </a>
        </div>

        <div class="collapse navbar-collapse" id="menu-toggle">
          <!-- <ul class="nav navbar-nav nav-menu"> -->
            <!-- <li class="active">
              <a class="page-scroll" href="#home">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="#o-que-fazemos">O que fazemos</a>
            </li>
            <li>
              <a class="page-scroll" href="#o-que-fizemos">O que fizemos</a>
            </li>
            <li>
              <a class="page-scroll" href="#blog">Blog</a>
            </li>
            <li>
              <a class="page-scroll" href="#contato">Contato</a>
            </li> -->

						<?php

								$defaults = array(
									'container' => false,
									'theme_location' => 'primary-menu',
									'menu_class' => 'nav navbar-nav nav-menu',

								);

								wp_nav_menu ( $defaults );

						?>

          <!-- </ul> -->
          <ul class="nav navbar-nav navbar-right nav-social">
            <li><a href="https://pt-br.facebook.com/granostudio/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/granostudio/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://www.linkedin.com/company/grano-studio"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </nav>
    </div>
  </header>
