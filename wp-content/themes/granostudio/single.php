<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header(); ?>

<body>



	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<section class="section section-post thumbnail2" id="post">

		<!-- <div class="container"> -->
			<!-- <div class="row"> -->
				<div class="">
					<div class="content-text mgb100" style="position: relative;margin-left: -15px;margin-right: -15px;">

							 <!-- thumbnail -->
							 <div class="thumb img-responsive img-post img-full post-animation"
							 							data-anchor-target="#post"
							 							data-top-bottom="opacity:1;transform:scale(1) translate3d(0px, -10%, 0px);transform-origin:50% 50%;"
							 							data-bottom-top="opacity:1;transform:scale(.96) translate3d(0px, 10%, 0px);transform-origin:50% 50%;"
														style="margin-bottom: 30px;margin-top: -16px;margin-left: -2px;">
														<?php if (has_post_thumbnail()): ?>
															<?php the_post_thumbnail( ); ?>
															<div class="mask"></div>
															<div class="col-md-6 col-md-offset-3 conteudo-thumbnail">
																<h1 class="titulo-single" data-anchor-target="#post"
																data-bottom-top="transform: translate3d(0px, 0%, 0px);"
																data-top-bottom="transform: translate3d(0px, -10%, 0px);"><?php echo get_the_title(); ?></h1>
																<ul class="post-info-top">
																	<!-- <li style="margin-bottom: 15px;"><a onClick="history.go(-1)" style="cursor: pointer;"><span class="arrow-left fa fa-long-arrow-left"></span> Voltar</a></li> -->
																	<li class="" style="text-align: center;padding-top: 10px;line-height: 20px;">
																		<span class="date"><?php echo get_the_date(); ?></span> <span class="divider">|</span> <span class="strong category"><?php echo the_category(); ?></span>
																	</li>
																</ul>
															 </div>
															</div>
															<!-- </div> -->
															</div>
														<!-- </div> -->
													</section>
														<section class="section section-post posts" id="post">

														<?php else: ?>
															<div class="col-md-6 col-md-offset-3 conteudo-thumbnail">
																<h1 class="titulo-single" data-anchor-target="#post"
																data-bottom-top="transform: translate3d(0px, 0%, 0px);"
																data-top-bottom="transform: translate3d(0px, -10%, 0px);"><?php echo get_the_title(); ?></h1>
																<ul class="post-info-top">
																	<!-- <li style="margin-bottom: 15px;"><a onClick="history.go(-1)" style="cursor: pointer;"><span class="arrow-left fa fa-long-arrow-left"></span> Voltar</a></li> -->
																	<li class="" style="text-align: center;padding-top: 10px;line-height: 20px;">
																		<span class="date" style="color: #51403a;"><?php echo get_the_date(); ?></span> <span class="divider" style="color: #51403a;">|</span> <span class="strong category"><?php echo the_category(); ?></span>
																	</li>
																</ul>
															 </div>
															</div>
															<!-- </div> -->
															</div>
														<!-- </div> -->
													</section>
														<section class="section section-post posts" id="post" style="margin-top: 230px;">
								            <?php endif; ?>

								<!-- </div> -->


			<!-- </div> -->
			<!-- </div> -->
		<!-- </div> -->
	<!-- </section>


	<section class="section section-post posts" id="post"> -->

		<!-- conteúdo -->
		<div class="content container">
			<div class="row content-center">
				<?php the_content(); ?>
			</div>
		</div>

		<!-- <div class="clearfix"></div> -->

		<div class="col-xs-12">
			<input type="button" value="Voltar" class="btn btn-enviar" onClick="history.go(-1)">
		</div>

	</section>

	<?php endwhile; // end of the loop. ?>

	<section style="background-color: #FAFAFA;">

	<section class="section section-1" id="section-1" style="background-color: #FAFAFA;">
			<div class="container">
				<div class="row content-search">
					<div class="col-xs-12">
							<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<div class="form-group">
								<div class="input-group input-group-unstyled">
									<input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
									<span class="input-group-addon">
										<i class="fa fa-search"></i>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

		</div>
	</section>
	<!-- <div class="clearfix"></div> -->

	<section class="section section-highlighted bg-cinza" id="highlighted" style="background-color: #FAFAFA;">
		<div class="container">
			<div class="row content-center">
				<div class="col-xs-12">
					<h2 class="title">Em destaque</h2>
				</div>
			</div>


			<div class="row portfolio-grid content-center mgt30">
					<?php

				 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				 $args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'paged' => $paged, 'page' => $paged);
				 $loop = new WP_Query( $args );

				 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


						<div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all" data-0="transform:scale(0.4);opacity:.6;" data-end="transform:scale(1);opacity:1;">
							<a href="<?php echo get_the_permalink(); ?>" class="image">
								<?php if (has_post_thumbnail()): ?>
		              <?php the_post_thumbnail( ); ?>
									<div class="caption">
										<span class="anima">
											<span class="icon grano-branco logo-grano">Grano</span>
											<span class="title"><?php echo get_the_title(); ?></span>
											<ul class="lista-categoria">
												<?php
												foreach((get_the_category()) as $category) {
													echo '<li class="hashtags">' . $category->cat_name . '</li>';
												}
												?>
											</ul>
										</span>
									</div>

		            <?php else: ?>
		              <div class="foto"></div>
									<div class="caption" style="background: none;">
										<span class="anima">
											<span class="icon grano-branco logo-grano">Grano</span>
											<span class="title"><?php echo get_the_title(); ?></span>
											<ul class="lista-categoria">
												<?php
												foreach((get_the_category()) as $category) {
													echo '<li class="hashtags">' . $category->cat_name . '</li>';
												}
												?>
											</ul>
										</span>
									</div>
		            <?php endif; ?>
							</a>
						</div>

			<?php endwhile; // end of the loop. ?>
		 	<?php endif; ?>

			</div>
			</div>
		</section>

	</section>
	<!-- <div class="clearfix"></div> -->



	<script src="<?php echo MEDIA_PATH; ?>/js/scripts.js"></script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=266321133860144";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<?php get_footer(); ?>
