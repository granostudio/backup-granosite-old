<?php
 
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

header("Content-Type: text/html; charset=utf-8",true);


if ($_SERVER['HTTP_HOST'] == '127.0.0.1'){
	define("ROOT_FOLDER", "/");
	// define("ROOT_PATH", "http://frontfe.com/client/seuemporio/public");
	define("ROOT_PATH", "");
	define("MEDIA_PATH", ROOT_PATH."grano-site/wp-content/themes/granostudio/media");

}elseif($_SERVER['HTTP_HOST'] == 'apps.frontfe.com'){
	define("ROOT_FOLDER", "/");
	define("ROOT_PATH", "http://apps.frontfe.com/grano-studio");
	define("MEDIA_PATH", ROOT_PATH."/media");
}


if(isset($_REQUEST['language'])){
	$lang		= $_REQUEST['language'];
}else{
	$lang		= substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	if($lang == "pt-br" || $lang == "pt-pt" || $lang == "pt"){
		$lang	= "br";
	}
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
	<title>Grano Studio - O que fizemos</title>

	<meta name="robots" content="index, follow" />

	<meta name="description" content="grano studio" />
	<meta name="keywords" content="studio, desenvolvimento, criacao, site, layout, design">
	<meta name="Abstract" content="Grano Studio" />

	<meta name="author" content="Grano Studio" />
	<meta name="copyright" content="Grano Studio" />


	<meta property="og:url" content="grano.com.br" />
	<meta property="og:title" content="Grano Studio" />
	<meta property="og:description" content="Grano Studio" />

	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Grano Studio" />


	<base href="<?php echo ROOT_PATH ?>/"  lang="<?php echo $lang ?>" />

	<link rel="shortcut icon" href="" />
	<meta property="og:image" content="<" />
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="200" />
	<meta property="og:image:height" content="200" />


	<link rel="stylesheet" href="<?php echo MEDIA_PATH; ?>/css/style.css" />
</head>

<body class="page-project">
	<header class="navbar-fixed-top" id="nav-main">
		<div class="container-fluid">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-toggle">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/logo.png" class="logo">
					</a>
				</div>

				<div class="collapse navbar-collapse" id="menu-toggle">
					<ul class="nav navbar-nav nav-menu">
						<li>
							<a class="page-scroll" href="#home">Home</a>
						</li>
						<li>
							<a class="page-scroll" href="#o-que-fazemos">O que fazemos</a>
						</li>
						<li class="active">
							<a class="page-scroll" href="#o-que-fizemos">O que fizemos</a>
						</li>
						<li>
							<a class="page-scroll" href="#blog">Blog</a>
						</li>
						<li>
							<a class="page-scroll" href="#contato">Contato</a>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right nav-social">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>

	<div id="sidebar-wrapper">
        <nav id="spy">
            <ul class="sidebar-nav-section nav nav-section">
                <li class="active">
                    <a href="#section-1" class="anchor">
                        <span class="txt">Serviços desenvolvidos</span><span class="icon progress-start"></span>
                    </a>
                </li>
                <li>
                    <a href="#section-2" class="anchor">
                        <span class="txt">Objetivo</span><span class="icon progress-1"></span>
                    </a>
                </li>
                <li>
                    <a href="#section-3" class="anchor">
                        <span class="txt">Diagnóstico</span><span class="icon progress-2"></span>
                    </a>
                </li>
                <li>
                    <a href="#section-4" class="anchor">
                        <span class="txt">Análise</span><span class="icon progress-3"></span>
                    </a>
                </li>
                <li class="finish">
                    <a href="#section-5" class="anchor">
                        <span class="txt">Resultado</span><span class="icon progress-end"></span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

	<section class="page-section section section-1" id="section-1">
		<div class="wrap-content">
			<div class="content-center">
				<div class="row sec-title">
					<div class="col-xs-12">
						<span class="icon progress-start"></span>
						<h3 class="txt-medium">Serviços desenvolvidos</h3>
						<p class="txt-sub">Cliente: Senses Cirurgia Plástica</p>
					</div>
				</div>

				<div class="services">
					<ul class="text-services" data-0="transform:scale(1);transform-origin:50% 50%;" data-200="transform:scale(1.2);transform-origin:50% 50%;">
						<li>Mudança de nome (Naming) <span class="divider">|</span> </li>
						<li>Branding & Identidade Visual</li>
						<li>Website responsivo <span class="divider">|</span></li>
						<li>Marketing Digital <span class="divider">|</span></li>
						<li>Materiais Impressos</li>
					</ul>

					<div class="clearfix"></div>

					<ul class="list-inline icons-services">
						<li data-0="transform:scale(0.7);opacity:.8;" data-30="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-4.png" /></li>
						<li data-20="transform:scale(0.7);opacity:.8;" data-70="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-2.png" /></li>
						<li data-60="transform:scale(0.7);opacity:.8;" data-120="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-3.png" /></li>
						<li data-110="transform:scale(0.7);opacity:.8;" data-180="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-5.png" /></li>
						<li data-170="transform:scale(0.7);opacity:.8;" data-230="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-1.png" /></li>
						<li data-220="transform:scale(0.7);opacity:.8;" data-280="transform:scale(1);opacity:1;"><img src="<?php echo MEDIA_PATH; ?>/imgs/icon-6.png" /></li>
					</ul>

					<span class="icon arrow-down bounce-arrow"></span>
				</div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="page-section section section-2" id="section-2">
		<div class="wrap-content">
			<div class="content-center">

				<div class="row sec-title">
					<div class="col-xs-12 project-animation" data-0="margin-bottom:100px" data-290="margin-bottom: 0;">
						<span class="icon progress-1"></span>
						<h3 class="txt-medium">Objetivo</h3>
						<p class="txt-sub">Cliente: Senses Cirurgia Plástica</p>
					</div>
				</div>

				<div class="content-text">
					<h3 class="txt-green mgt100 project-animation" data-top-bottom="top:0;margin-top:50px;" data-bottom-top="top:-50px;">Gerar oportunidades de negócio e aumentar a receita da empresa</h3>

					<span class="icon arrow-down bounce-arrow"></span>
				</div>
			</div>

		</div>
	</section>
	<div class="clearfix"></div>


	<section class="page-section section section-3" id="section-3">
		<div class="container">
			<div class="row mgt50 mgb50 sec-title">
				<div class="col-xs-12 project-animation" data-0="margin-bottom:100px" data-bottom-top="margin-bottom: 0;">
					<span class="icon progress-2"></span>
					<h3 class="txt-medium">Diagnóstico</h3>
					<p class="txt-sub">Cliente: Senses Cirurgia Plástica</p>
				</div>
			</div>

			<div class="slides project-animation" data-bottom-top="opacity:.8;transform:scale(1.2);transform-origin:50% 50%;margin-top:20px;" data-top-bottom="opacity:1;transform:scale(1);transform-origin:50% 50%;">
				<div id="carousel-diagnostico" class="carousel">
					<div class="item" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod">
						<a href="" class="link">
							<img src="http://via.placeholder.com/350x350" class="rz">
						</a>
					</div>
					<div class="item" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod">
						<a href="" class="link">
							<img src="http://via.placeholder.com/350x350" class="rz">
						</a>
					</div>
					<div class="item" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod">
						<a href="" class="link">
							<img src="http://via.placeholder.com/350x350" class="rz">
						</a>
					</div>
				</div>
				<div class="clearfix"></div>

				<div class="caption"></div>
				<ul id="nav" class="nav-dots"></ul>
			</div>

			<div class="clearfix"></div>
			<span class="icon arrow-down bounce-arrow mgt50 mgb50"></span>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="page-section section section-4" id="section-4">
		<div class="container">
			<div class="row mgt50 mgb50 sec-title">
				<div class="col-xs-12 project-animation" data-0="margin-bottom:100px" data-bottom-top="margin-bottom: 0;">
					<span class="icon progress-3"></span>
					<h3 class="txt-medium">Análise</h3>
					<p class="txt-sub">Cliente: Senses Cirurgia Plástica</p>
				</div>
			</div>

			<div class="row text-numbers">
				<div class="col-xs-12 col-sm-4 item box-green">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">38,6%</div>
					<p>Aumento no público adulto em busca por cirurgias plásticas</p>
				</div>
				<div class="col-xs-12 col-sm-4 item box-green">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">141%</div>
					<p>Aumento no público adolescente (14 a 18) em busca por cirurgias plásticas</p>
				</div>
				<div class="col-xs-12 col-sm-4 item box-green">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">13-15%</div>
					<p>Do orçamento é investido em saúde</p>
				</div>
				<div class="col-xs-12 col-sm-4 item box-yellow">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">87,2%</div>
					<p>Mulheres</p>
				</div>
				<div class="col-xs-12 col-sm-4 item box-yellow">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">12,8%</div>
					<p>Homens</p>
				</div>
				<div class="col-xs-12 col-sm-4 item box-yellow">
					<div class="txt-percent" data-bottom-top="font-size: 25px;" data-top-bottom="font-size: 55px;">10,5%</div>
					<p>Aumento do público masculino</p>
				</div>
			</div>

			<span class="icon arrow-down bounce-arrow"></span>
		</div>
	</section>
	<div class="clearfix"></div>


	<section class="page-section section section-5" id="section-5">
		<div class="container">
			<div class="row mgt50 mgb50 sec-title">
				<div class="col-xs-12 project-animation" data-0="margin-bottom:100px" data-bottom-top="margin-bottom: 0;">
					<span class="icon progress-end"></span>
					<h3 class="txt-medium">Materiais desenvolvidos</h3>
					<p class="txt-sub">Cliente: Senses Cirurgia Plástica</p>
				</div>
			</div>

			<div class="row grid-portfolio">
				<div class="col-xs-12">
					<div class="cases-container">
						<div class="post-sizer"></div>

						<div class="post-item post-item--width2 post-item--height2" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/anahp-15-anos-a-historia-da-assistencia-hospitalar-e-do-sistema-de-saude-no-brasil/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2016/10/Anahp-1-1024x682.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">Anahp 15 anos &#8211; A história da assistência hospitalar e do sistema de Saúde no Brasil</span>
									</div>
								</a>
							</article>
						</div>

						<div class="post-item post-item--height2" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/imagem-pessoal-conteudo-autentico-e-de-alto-impacto-para-lideres/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2016/10/medico-1024x682.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">Imagem pessoal: conteúdo autêntico e de alto impacto para líderes</span>
									</div>
								</a>
							</article>
						</div>

						<div class="post-item post-item--full" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/network1-e-essense-conteudo-como-base-para-gerar-negocios/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2016/10/conteudos-que-geram-negocios-1024x615.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">Network1 e essense: conteúdo como base para gerar negócios</span>
									</div>
								</a>
							</article>
						</div>

						<div class="post-item" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/2s-e-essense-conteudo-multiplataforma-para-engajar-lideres-de-ti/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2016/10/multiplataforma-1024x682.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">2S e essense: conteúdo multiplataforma para engajar líderes de TI</span>
									</div>
								</a>
							</article>
						</div>

						<div class="post-item" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/comptia-brasil-e-essense-conteudo-para-engajar-profissionais-de-ti/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2015/10/suaVoz-1024x682.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">CompTIA Brasil e essense: conteúdo para engajar profissionais de TI</span>
									</div>
								</a>
							</article>
						</div>

						<div class="post-item" data-bottom-top="opacity:.5;" data-top-bottom="opacity:1;">
							<article class="item-case">
								<a href="http://www.agenciaessense.com.br/novosite/2016/10/04/comptia-brasil-e-essense-conteudo-para-engajar-profissionais-de-ti/" title="nome do post" class="link-post" data-id="">
									<img src="http://www.agenciaessense.com.br/novosite/wp-content/uploads/2015/10/suaVoz-1024x682.jpg" alt="Name" class="rz img-post">
									<div class="post-caption">
										<span class="line post-title">CompTIA Brasil e essense: conteúdo para engajar profissionais de TI</span>
									</div>
								</a>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<section class="page-section section section-6" id="section-6">
	<div class="container">
		<div class="row mgt50 mgb50 sec-title">
			<div class="col-xs-12">
				<h3 class="txt-medium">Mais cases</h3>
			</div>
		</div>

		<div class="row grid-portfolio">
			<div class="col-xs-12 col-sm-4 item" data-0="transform:scale(0.4);opacity:.6;" data-end="transform:scale(1);opacity:1;">
				<a href="">
					<img src="http://via.placeholder.com/350x500" class="rz">
				</a>
			</div>
			<div class="col-xs-12 col-sm-4 item" data-0="transform:scale(0.4);opacity:.6;" data-end="transform:scale(1);opacity:1;">
				<a href="">
					<img src="http://via.placeholder.com/350x500" class="rz">
				</a>
			</div>
			<div class="col-xs-12 col-sm-4 item" data-0="transform:scale(0.4);opacity:.6;" data-end="transform:scale(1);opacity:1;">
				<a href="">
					<img src="http://via.placeholder.com/350x500" class="rz">
				</a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<a href="" class="btn btn-enviar">Voltar para a Home</a>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<script src="<?php echo MEDIA_PATH; ?>/js/scripts.js"></script>

</body>
