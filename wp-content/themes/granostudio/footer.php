<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since GranoStudio 2.0
 */
?>
<?php wp_footer(); ?>

</body>

</html>
